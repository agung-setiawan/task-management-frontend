@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Task -->
    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Task</div>
                <div class="card-body">
                    <table class="tbls table">
                        <thead>
                            <tr>
                                <th width="2%">No.</th>
                                <th width="20%">Title</th>
                                <th width="20%">Description</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Status</th>
                                <th>Assignee</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection