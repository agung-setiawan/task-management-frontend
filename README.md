## About Laravel

Task management frontend is part of task management project to have function to show or display task list in front end

## How to Install

- Git clone to local: git clone https://agung-setiawan@bitbucket.org/agung-setiawan/task-management-frontend.git.
- Make sure backend part is already running.
- Open terminal.
- Execute command : php artisan serve --port=5000.
- Open Browser and pointing to URL http://localhost:5000/

## Creator

Agung Setiawan

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
